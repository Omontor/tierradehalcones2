<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('feedbacks','FeedbackController');
Route::resource('users','UsersController');
Route::get('users/create','UsersController@create');

Route::get('users/{id}/report', 'ReportController@userReport');
Route::get('users/global-questions-report/download','ReportController@globalQuestionsUsers');
Route::get('users/global-bubbles-report/download','ReportController@globalBubblesUsers');
Route::get('users/global-minigames-report/download','ReportController@globalMinigamesUsers');
Route::get('users/reported-gameovers/download', 'ReportController@usersGameover');
Route::get('users/report-top-ten/download', 'ReportController@reportTopUsers');
Route::get('users/general-report/download', 'ReportController@generalReport');

Route::resource('notifications','NotificationsController');
Route::get('globalStats', 'UsersController@viewGlobalStats');
Route::get('usersList', 'UsersController@usersList');
Route::get('globalStatsQuestions', 'UsersController@globalStatsQuestions');
Route::get('globalStatsQuestionsByCategories', 'UsersController@globalStatsQuestionsByCategory');
Route::get('globalStatsBubbles', 'UsersController@globalStatsBubbles');
Route::get('global-stats-dynamic-minigames', 'UsersController@globalStatsdynamicMinigames');

Route::get('toptens','UsersController@topTenList');
Route::get('topTen','UsersController@viewTopTen');
Route::get('feedbacksList','FeedbackController@feedbacksList');

Route::get('regions', 'LocationController@regionsList');
Route::get('regions/{regionId}/districts', 'LocationController@districtsList');
Route::get('regions/{regionId}/districts/{districtId}/sectors', 'LocationController@sectorsList');
Route::get('sectors', 'LocationController@sectors');

Route::post('dynamic-minigames','DynamicMinigameController@store');
Route::get('dynamic-minigames/view','DynamicMinigameController@viewMinigames');
Route::get('dynamic-minigames','DynamicMinigameController@listMinigames');
Route::get('dynamic-minigames/create','DynamicMinigameController@viewCreation');

Route::post('/create-users-excel','ExcelController@registerUsers');
Route::post('/delete-users-excel','ExcelController@deleteUsers');
Route::get('/file-operations','ExcelController@filesView');

Route::get('/register-areas', 'ExcelController@registerLocations');
Route::get('/fill-sector-users', 'ExcelController@fillSectorToUsers');
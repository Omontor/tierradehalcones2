<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/users','UsersController@store');
Route::resource('/questions','Api\QuestionsController');
Route::resource('/bubbles','Api\BubblesController');
Route::resource('/minigames','Api\MiniGamesController');
Route::resource('/minigames','Api\MiniGamesController');
Route::resource('/feedbacks','Api\FeedbacksController');
Route::get('/topten','Api\UsersController@topTenList');
Route::post('/notifications/game-over', 'Api\NotificationsController@gameOver');

Route::post('/targets', 'Api\DynamicMinigamesResultsController@store');
Route::get('/dynamic-minigames','Api\DynamicMinigamesResultsController@index');
Route::get('/dynamic-minigames/{id}','Api\DynamicMinigamesResultsController@show');

Route::get('/dynamic-minigames-catalogue', 'Api\DynamicMinigameController@listMinigames');
Route::get('/dynamic-minigames-catalogue/{id}', 'Api\DynamicMinigameController@show');

Route::post('/videos', 'Api\VideosController@store');
Route::get('/videos', 'Api\VideosController@index');
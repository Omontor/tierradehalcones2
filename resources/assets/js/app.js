import Vue from 'vue';
import VueHighcharts from 'vue-highcharts';
import Highcharts from 'highcharts';
import loadExporting from 'highcharts/modules/exporting';
loadExporting(Highcharts);
import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'

Vue.use(VueHighcharts);
Vue.use(VueGoodTablePlugin);

window.Popper = require('popper.js').default;
require('./bootstrap');

window.Vue = require('vue');

Vue.component('userslist', require('./components/Users.vue'));
Vue.component('feedbackslist', require('./components/Feedbacks.vue'));
Vue.component('toptens', require('./components/Topten.vue'));
Vue.component('piechart', require('./components/PieChart.vue'));
Vue.component('columnchart', require('./components/ColumnChart.vue'));
Vue.component('globalstats', require('./components/GlobalStats.vue'));
Vue.component('newuser', require('./components/NewUser.vue'));
Vue.component('newdynamicminigame', require('./components/NewDynamicMinigame.vue'));
Vue.component('dynamicminigames', require('./components/DynamicMinigames'));
Vue.component('fileoperations', require('./components/FileOperations'));

const app = new Vue({
    el: '#app'
});

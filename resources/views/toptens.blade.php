@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <h4>Los 100 usuarios con Scores más altos</h4>
                <toptens />
            </div>
        </div>
    </div>
@endsection

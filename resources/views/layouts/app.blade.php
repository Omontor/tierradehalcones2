<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Tierra de Halcones') }}</title>

    <!-- Styles -->
    <script src="https://use.fontawesome.com/0751f277ca.js"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

</head>
<body>
    <div id="app">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              {{-- <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> --}}
            </li>
            @if (Auth::guest())
              {{-- <li class="nav-item ">
                <a class="nav-link" href="{{ route('login') }}">Login</a>
              </li> --}}
              {{-- <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">Register</a>
              </li> --}}
              @else
                <li class="nav-item {{\Request::is('home') ? 'active' : ''}}">
                  <a class="nav-link" href="{{ url('/home') }}">
                      <i class="fas fa-chart-pie fa-lg"></i>
                  </a>
                </li>
                  <li class="nav-item {{\Request::is('usersList') ? 'active' : ''}}">
                      <a class="nav-link" href="{{ url('/usersList') }}">
                          <i class="fas fa-users fa-lg"></i>
                      </a>
                  </li>
               {{-- <li class="nav-item {{\Request::is('globalStats') ? 'active' : ''}}">
                  <a class="nav-link" href="{{ url('/globalStats') }}">Gráfica general</a>
                </li>--}}
                <li class="nav-item {{\Request::is('topTen') ? 'active' : ''}}">
                  <a class="nav-link" href="{{ url('topTen') }}">
                      <i class="fas fa-trophy"></i>
                  </a>
                </li>
                <li class="nav-item {{\Request::is('feedbacksList') ? 'active' : ''}}">
                  <a class="nav-link" href="{{ url('feedbacksList') }}">
                      <i class="fas fa-comments fa-lg"></i>
                  </a>
                </li>
                  <li class="nav-item {{\Request::is('notifications') ? 'active' : ''}}">
                      <a class="nav-link" href="{{ url('notifications') }}">
                          <i class="fas fa-bell fa-lg"></i>
                      </a>
                  </li>
                  <li class="nav-item {{\Request::is('users/create') ? 'active' : ''}}">
                      <a class="nav-link" href="{{ url('users/create') }}">
                          <i class="fas fa-user-plus fa-lg"></i>
                      </a>
                  </li>
                  <li class="nav-item {{\Request::is('dynamic-minigames/create') ? 'active' : ''}}">
                      <a class="nav-link" href="{{ url('dynamic-minigames/create') }}">
                          <i class="fas fa-gamepad fa-lg"></i>
                      </a>
                  </li>
                  <li class="nav-item {{\Request::is('dynamic-minigames/view') ? 'active' : ''}}">
                      <a class="nav-link" href="{{ url('dynamic-minigames/view') }}">
                          <i class="fas fa-th-list"></i>
                      </a>
                  </li>
                  <li class="nav-item {{\Request::is('/file-operations') ? 'active' : ''}}">
                      <a class="nav-link" href="{{ url('/file-operations') }}">
                          <i class="fas fa-file-excel"></i>
                      </a>
                  </li>
            @endif
          </ul>
          @if (!Auth::guest())
            <ul class="navbar-nav ml-auto">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ Auth::user()->name }}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();"
                  >Logout</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          @endif
        </div>
      </nav>
        <!--Errors sections -->
        @if(Session::has('status'))
            @if(Session::get('status'))
                <div class="alert alert-success text-center" role="alert"  >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>{{ Session::get('mess')}}</strong>
                </div>
            @elseeve
                <div class="alert alert-danger text-center" role="alert" >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('mess')}}
                </div>
            @endif
        @endif
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js').'?key='.time() }}"></script>
</body>
</html>

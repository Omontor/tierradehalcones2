@extends('layouts.app')

@section('content')
    <div class="container">
       <div class="row">
           <div class="col-md-3"></div>
           <div class="col-6">
               <h3>Envia una notificación</h3>
               <form method="POST" action="{{url('notifications')}}">
                   {{ csrf_field() }}
                   <div class="form-group">
                       <label for="title">Titulo</label>
                       <input type="text" class="form-control" id="title" name="title" placeholder="Titulo de notificación" required>
                   </div>
                   <div class="form-group">
                       <label for="message">Mensaje</label>
                       <textarea class="form-control" name="message" id="" cols="20" rows="10" required></textarea>
                   </div>
                   <button type="submit" class="btn btn-primary btn-block">Enviar</button>
               </form>
           </div>
       </div>
    </div>
@endsection

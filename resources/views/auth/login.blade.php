@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-3">

      </div>
        <div class="col-6" style="margin-top:50px;">
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}
              @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                  @foreach ($errors->all() as $error)
                      <p class="text-center">{{ $error }}</li>
                  @endforeach
                </div>
              @endif
              <div class="form-group{{ $errors->has('email') ? 'is-invalid' : '' }}">
                  <label for="email" class="control-label">E-Mail Address</label>

                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
              </div>

              <div class="form-group{{ $errors->has('password') ? ' is-invalid' : '' }}">
                  <label for="password" class="control-label">Password</label>

                  <input id="password" type="password" class="form-control" name="password" required>

                  @if ($errors->has('password'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>

            {{--  <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                          </label>
                      </div>
                  </div>
              </div>--}}

              <div class="form-group">
                <button type="submit" class="btn btn-primary ">
                    Login
                </button>


                    {{--  <a class="btn btn-link" href="{{ route('password.request') }}">
                          Forgot Your Password?
                      </a>--}}

              </div>
          </form>
        </div>
    </div>
</div>
@endsection

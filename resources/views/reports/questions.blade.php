<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <table>
            <tr>
                <td style="font-weight: bold; font-size: 16;">{{$user->name}}</td>
                <td style="font-weight: bold; font-size: 16;">{{$user->email}}</td>
            </tr>
        </table>
        <tbody>
            @foreach($questionsByCategory as $key => $questions)
                @php
                    $rightCategoryQuestions = $questions->reduce(function ($tot, $q) {
                        if ($q->answer === 1){
                        return $tot + 1;
                        }
                        return $tot;
                    }, 0);
                @endphp
                <tr>
                    <th style="font-size: 14;">
                        {{$key}} {{$rightCategoryQuestions}} de {{$categories[$key]['total']}}
                    </th>
                </tr>
                <tr>
                    <th style="text-align: center; color: #8f8f8f;">Pregunta</th>
                    <th style="text-align: center; color: #8f8f8f;">Respuesta</th>
                </tr>
                @foreach($questions as $q)
                    <tr>
                        <td style="text-align: center; background-color: #cbcbcb">{{$q->idQuestion}}</td>
                        <td style="text-align: center; color:blue">{{($q->answer == 1) ? '✔': '✖'}}</td>
                    </tr>
                @endforeach

            @endforeach
            <tr>
                <th style="text-align: center;">Total</th>
                <th style="text-align: center;">{{$rightQuestions}}</th>
            </tr>
        </tbody>
    </body>
</html>

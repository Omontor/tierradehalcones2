<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <table>
          @php
            $numQuestions = collect([]);
          @endphp
            <tr>
                <td style="font-weight: bold; font-size: 16;">Id</td>
                <td style="font-weight: bold; font-size: 16;">Nombre</td>
                <td style="font-weight: bold; font-size: 16;">Email</td>
                @for ($i=1; $i <= 40; $i++)
                  @php $numQuestions->push($i); @endphp
                  <td style="font-weight: bold; font-size: 16;">{{$i}}</td>
                @endfor
                <td style="font-weight: bold; font-size: 16;">MERCADEOS</td>
                <td style="font-weight: bold; font-size: 16;">DISPONIBILIDAD</td>
                <td style="font-weight: bold; font-size: 16;">CRITERIOS DE EXHIBICIÓN</td>
                <td style="font-weight: bold; font-size: 16;">SEGURIDAD</td>
                <td style="font-weight: bold; font-size: 16;">OMS</td>
            </tr>
        </table>
        <tbody>

          @foreach ($users as $user)
            @php
                $questionsByCategory = $user->questions->groupBy('category');
            @endphp
            <tr>
              <td>{{$user->id}}</td>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              @foreach ($numQuestions as $q)
                @php
                  $isQuestionExistsAndTrue = $user->questions->contains(function($question, $key) use($q){
                    return $question->idQuestion == $q && $question->answer == 1;
                  });
                @endphp
                @if ($isQuestionExistsAndTrue)
                  <td style="text-align: center; color:blue;">
                    {{'✔'}}
                  </td>
                @else
                  <td style="text-align: center; color:blue;">
                    {{'✖'}}
                  </td>
                @endif
              @endforeach

              @foreach($questionsByCategory as $key => $questions)
                @php
                  $rightCategoryQuestions = $questions->reduce(function ($tot, $q) {
                      if ($q->answer == 1){
                      return $tot + 1;
                      }
                      return $tot;
                  }, 0);
                @endphp
                <th style="font-size: 14;">
                    {{$rightCategoryQuestions}} de {{$categories[$key]['total']}}
                </th>
              @endforeach
            </tr>
          @endforeach
        </tbody>
    </body>
</html>

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocationsSeeder::class);
        $this->call(JediSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(FeedbackSeeder::class);
        $this->call(DynamicMinigameSeeder::class);
        $this->call(VideoCatalogSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = ['TECH', 'MAINTAINANCE', 'ART', 'HEALTH', 'FUN', 'SCHOOL'];
        foreach ($groups as $group){
            \App\Group::create([
                'name' => $group
            ]);
        }
    }
}

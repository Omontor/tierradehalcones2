<?php

use Illuminate\Database\Seeder;

class DynamicMinigameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $searchBubble = \App\DynamicMinigameCatalogue::create([
            'name' => 'Test Busqueda de Burbijas',
            'maturityDate' => Carbon\Carbon::now()->addMonth(),
            'duration' => 40, //min
            'gameType' => 'SEARCH_BUBBLES',
            'unitNumber' => 30,
            'unitType' => 'BUBBLES'
        ]);

        $searchObjects = \App\DynamicMinigameCatalogue::create([
            'name' => 'Test Busqueda de Objetos',
            'maturityDate' => Carbon\Carbon::now()->addDays(15),
            'duration' => 20, //min
            'gameType' => 'SEARCH_OBJECTS',
            'unitNumber' => 40,
            'unitType' => 'OBJECTS'
        ]);

        $answerQuestions = \App\DynamicMinigameCatalogue::create([
            'name' => 'Test Responder preguntas',
            'maturityDate' => Carbon\Carbon::now()->addDays(40),
            'duration' => 50, //min
            'gameType' => 'ANSWER_QUESTIONS',
            'unitNumber' => 20,
            'unitType' => 'QUESTIONS'
        ]);

        $question = \App\MinigameQuestion::create([
            'text' => '¿Iphone o Android?',
            'dynamic_minigame_catalogue_id' => $answerQuestions->id
        ]);

        $answer = \App\Answer::create([
            'text' => 'Android',
            'isAnswer' => false,
            'minigame_question_id' => $question->id
        ]);
        $answer = \App\Answer::create([
            'text' => 'Iphone',
            'isAnswer' => true,
            'minigame_question_id' => $question->id
        ]);
        $question2 = \App\MinigameQuestion::create([
            'text' => '¿Y el perrito ?',
            'dynamic_minigame_catalogue_id' => $answerQuestions->id
        ]);
        $answer = \App\Answer::create([
            'text' => 'No se',
            'isAnswer' => false,
            'minigame_question_id' => $question2->id
        ]);
        $answer = \App\Answer::create([
            'text' => 'Esta conmigo :D',
            'isAnswer' => true,
            'minigame_question_id' => $question2->id
        ]);

    }
}

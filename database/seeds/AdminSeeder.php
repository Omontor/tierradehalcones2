<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
        'name' => 'Mark',
        'email' => 'super@tierra.com',
        'rol' => 'admin',
        'password' =>  Hash::make('supermark')
      ]);
      User::create([
        'name' => 'Oliver',
        'email' => 'superOliver@tierra.com',
        'rol' => 'admin',
        'password' =>  Hash::make('nomelase!!')
      ]);
    }
}

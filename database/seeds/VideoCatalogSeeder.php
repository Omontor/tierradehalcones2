<?php

use Illuminate\Database\Seeder;

class VideoCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\VideoCatalog::create([
            'catalogType' => 'COACHING',
            'name' => 'Feedback',
            'maxNumber'=> 40
        ]);
        \App\VideoCatalog::create([
            'catalogType' => 'ELEARNING',
            'name' => 'E-Learning',
            'maxNumber'=> 40
        ]);
        \App\VideoCatalog::create([
            'catalogType' => 'TRAINING',
            'name' => 'Inducción',
            'maxNumber'=> 20
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;


class FeedbackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i < 300; $i++) {
          \App\Feedback::create(['feedback' => $faker->realText(rand(30,100))]);
        }
    }
}

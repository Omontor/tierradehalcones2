<?php

use Illuminate\Database\Seeder;

class JediSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $sectors = \App\Sector::all();
        $sectors->each(function ($sector) use($faker){
           \App\Jedi::create([
               'name' => $faker->firstName . ' ' . $faker->lastName,
               'sector_id' => $sector->id
           ]);
        });
    }
}

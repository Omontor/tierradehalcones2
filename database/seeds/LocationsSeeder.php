<?php

use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [[
            'region' => 'MX5001',
            'districts' => [
                [
                    'name'=>'MX500101',
                    'sectors' => [
                        'MX50010101','MX50010102','MX50010103','MX50010104',
                        'MX50010105','MX50010106','MX50010107','MX50010108',
                        'MX50010109','MX50010110','MX50010111','MX50010112',
                        'MX50010113','MX50010114'
                    ]
                ],
                [
                    'name'=> 'MX500102',
                    'sectors' => [
                        'MX50010201','MX50010202','MX50010203','MX50010204',
                        'MX50010205','MX50010206','MX50010207'
                    ]
                ],
            ],
        ],
            [
                'region' => 'MX5002',
                'districts' => [
                    [
                        'name'=>'MX500201',
                        'sectors' => [
                            'MX50020101','MX50020102','MX50020103','MX50020104',
                            'MX50020105', 'MX50020106','MX50020107','MX50020108',
                            'MX50020109','MX50020110'
                        ]
                    ],
                    [
                        'name'=>'MX500202',
                        'sectors' => [
                            'MX50020201','MX50020202','MX50020203','MX50020204',
                            'MX50020205','MX50020206','MX50020207','MX50020208'
                        ]
                    ],
                    [
                        'name'=>'MX500203',
                        'sectors' => [
                            'MX50020301','MX50020302','MX50020303','MX50020304',
                            'MX50020305','MX50020306','MX50020307'
                        ]
                    ]
                ]
            ],
            [
                'region' => 'MX5003',
                'districts' => [
                    [
                        'name'=>'MX500301',
                        'sectors' => [
                            'MX50030101','MX50030102','MX50030103','MX50030104',
                            'MX50030105','MX50030106','MX50030107'
                        ]
                    ],
                    [
                        'name'=>'MX500302',
                        'sectors' => [
                            'MX50030201','MX50030202','MX50030203','MX50030204',
                            'MX50030205','MX50030206','MX50030207','MX50030208'
                        ]
                    ]
                ]
            ]];

        foreach ($locations as $region){
            $regionCreated = \App\Region::create(['code' => $region['region'], 'name' => $region['region']]);
            foreach ($region['districts'] as $district){
                $districtCreated = \App\District::create(['code' => $district['name'], 'name' => $district['name'], 'region_id' => $regionCreated->id]);
                foreach ($district['sectors'] as $sector){
                    $sectorCreated = \App\Sector::create(['code' => $sector, 'name' => $sector, 'district_id' => $districtCreated->id]);
                }
            }
        }
    }
}

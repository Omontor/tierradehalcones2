<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $questionCategory = config('allQuestionsCategory');
        factory(App\User::class, 800)->create()->each(function ($u) use ($faker){});
        $users = \App\User::all();

        foreach ($users as $u) {
            $numQuestions = $faker->numberBetween(5,40);
            for ($i = 1 ; $i <= $numQuestions; $i++){
                $question = new \App\Question([
                    'idQuestion' => $i,
                    'answer' => filter_var($faker->numberBetween(0,1), FILTER_VALIDATE_BOOLEAN),
                    'category' => $questionCategory[$i]
                ]);
                $u->questions()->save($question);
                $question->updateScore($u);
            }
            $numBubbles = $faker->numberBetween(10,40);
            for ($i = 1 ; $i<=$numBubbles; $i++){
                $bubble =new \App\Bubble([
                    'idBubble' => $i
                ]);
                $u->bubbles()->save($bubble);
                $bubble->updateScore($u);
            }
            $numMinigames = $faker->numberBetween(1,8);
            for ($i = 1 ; $i<=$numMinigames; $i++){
                $minigame = new \App\Minigame([
                    'idMinigame' => $i
                ]);
                $u->minigames()->save($minigame);
                $minigame->updateScore($u);
            }
        }
    }
}

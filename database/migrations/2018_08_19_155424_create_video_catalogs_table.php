<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_catalogs', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('catalogType', ['COACHING','ELEARNING', 'TRAINING']);
            $table->string('name');
            $table->integer('maxNumber')->comment('total of videos or animations');
            $table->boolean('isEnable')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_catalogs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicMinigamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_minigames', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unitType');
            $table->integer('score')->default(0);
            $table->integer('totalTargets')->default(0);

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('dynamic_minigame_catalogue_id')->unsigned();
            $table->foreign('dynamic_minigame_catalogue_id')->references('id')->on('dynamic_minigames_catalogue');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_minigames');
    }
}

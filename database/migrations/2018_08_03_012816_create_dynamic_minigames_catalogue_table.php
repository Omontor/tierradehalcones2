<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicMinigamesCatalogueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_minigames_catalogue', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('maturityDate')->comment('Date when the game will no be available');
            $table->integer('duration')->comment('Duration for the game');
            $table->boolean('isActive')->comment('Indicator for the game availability')->default(true);
            $table->enum('gameType', ['SEARCH_BUBBLES', 'ANSWER_QUESTIONS','SEARCH_OBJECTS']);
            $table->integer('unitNumber')->comment('Max amount number of objects which have to be completed in the game');
            $table->string('unitType');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_minigames_catalogue');
    }
}

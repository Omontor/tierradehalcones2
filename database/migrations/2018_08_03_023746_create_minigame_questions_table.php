<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinigameQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minigame_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text');

            $table->integer('dynamic_minigame_catalogue_id')->unsigned();
            $table->foreign('dynamic_minigame_catalogue_id')->references('id')->on('dynamic_minigames_catalogue');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minigame_questions');
    }
}

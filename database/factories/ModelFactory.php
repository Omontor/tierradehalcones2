<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $hasSector = $faker->randomElement([true, false]);
    return [
        'name' => $faker->name,
        'completeName' => $faker->name. $faker->firstName . $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'sector_id' => ($hasSector) ? $faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]) : null,
        'videosSeenNumber' => $faker->randomNumber(2),
        'dynamicMinigamesScore' => $faker->randomNumber(4)
    ];
});

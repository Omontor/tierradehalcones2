<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicMinigame extends Model
{
    protected $table = 'dynamic_minigames';
    protected $fillable = ['unitType','score','totalTargets','user_id','dynamic_minigame_catalogue_id'];
    protected $hidden = ['created_at', 'updated_at', 'dynamic_minigame_catalogue_id','user_id'];
    protected $appends = ['unitTypeName'];

    public function dynamicMinigameCatalogue()
    {
        return $this->belongsTo(DynamicMinigameCatalogue::class);
    }

    public function player()
    {
        return $this->belongsTo(User::class);
    }

    public function targets()
    {
        return $this->hasMany(Target::class, 'dynamic_minigames_id');
    }

    /**
     * @internal updating user bubble score
     * @return int
     */
    public function updateScore()
    {
        $this->score += 1;
        $this->save();
        return $this->score;
    }

    public function getUnitTypeNameAttribute()
    {
        switch ($this->unitType){
            case 'BUBBLES':
                return 'Burbujas';
                break;
            case 'QUESTIONS':
                return 'Preguntas';
                break;
            case 'OBJECTS':
                return 'Objetos';
                break;
            default:
                return 'Units';
        }
    }
}

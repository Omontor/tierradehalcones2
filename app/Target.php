<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $fillable = ['targetId', 'wasAchieved', 'dynamic_minigames_id'];
    protected $hidden = ['created_at', 'updated_at', 'dynamic_minigames_id', 'wasAchieved'];

    public function dynamicMinigame()
    {
        return $this->belongsTo(DynamicMinigame::class);
    }
}

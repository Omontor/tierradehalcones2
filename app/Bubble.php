<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bubble extends Model
{
    protected $fillable = ['idBubble','user_id'];
    protected $hidden = ['updated_at', 'created_at'];
    protected $casts = [ 'idBubble' => 'integer' ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @internal updating user bubble score
     * @return int
     */
    public function updateScore(User $user)
    {
        $user->bubbleScore += 10;
        $user->save();
        $user->updateScore();
        return $user->bubbleScore;
    }

    public static function percentagesRecollectedBubbles()
    {
      $numBubblesCollected = Bubble::count();
      $users = User::where('rol','user')->count();

      $supposedBubbles = $users * 40;
      $missingBubbles = $supposedBubbles - $numBubblesCollected;

      $recollectedPercentage = $numBubblesCollected * 100 / $supposedBubbles;
      $missingPercentage = 100 - $recollectedPercentage;

      return [
        'numBubblesCollected' => $numBubblesCollected,
        'missingBubbles' => $missingBubbles,
        'supposedBubbles' => $supposedBubbles,
        'users' => $users,
        'recollectedPercentage' => $recollectedPercentage,
        'missingPercentage' => $missingPercentage
      ];
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'rol' ,'password', 'questionsScore', 'bubbleScore', 'minigamesScore',
        'score', 'sector_id', 'completeName', 'hasLoggedIn', 'videosSeenNumber', 'dynamicMinigamesScore', 'status'
    ];
    protected $casts = [
        'questionsScore' => 'integer',
        'bubbleScore' => 'integer',
        'minigamesScore' => 'score',
        'questionsScore' => 'integer'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'updated_at',
    ];


    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function bubbles()
    {
        return $this->hasMany(Bubble::class);
    }
    public function minigames()
    {
        return $this->hasMany(Minigame::class);
    }

    public function dynamicMinigames()
    {
        return $this->hasMany(DynamicMinigame::class);
    }

    public  function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function updateScore()
    {
        $this->score = $this->questionsScore + $this->bubbleScore + $this->minigamesScore + $this->dynamicMinigamesScore;
        $this->save();

        return $this->score;
    }

    public function dynamicMinigamesScore($gameType)
    {
        $this->dynamicMinigamesScore = $this->dynamicMinigamesScore + 1;
        $this->save();

        $this->updateScore();

        return $this->dynamicMinigamesScore;
    }
}

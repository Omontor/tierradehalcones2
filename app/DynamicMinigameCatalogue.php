<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicMinigameCatalogue extends Model
{
    protected $table = 'dynamic_minigames_catalogue';
    protected $fillable = ['name', 'maturityDate', 'duration', 'isActive', 'gameType', 'unitNumber', 'unitType'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = [
        'duration' => 'integer',
        'unitNumber' => 'integer',
        'isActive' => 'boolean'
    ];
    protected $appends = ['nameGameType'];

    public function dynamicGames()
    {
        return $this->hasMany(DynamicMinigame::class);
    }

    public function questions()
    {
        return $this->hasMany(MinigameQuestion::class);
    }

    public function isAnswerQuestionType()
    {
        return $this->gameType === 'ANSWER_QUESTIONS';
    }

    /**
     * This function get a unit type throw the game type which contains
     * the object which will be handleling in the minigame
     * some examples are : OBJECTS , QUESTIONS, BUBBLES
     * @param $gameType
     * @return string
     */
    public static function extractUnitType($gameType)
    {
        $explode = explode("_", $gameType);
        return $explode[1];
    }

    public function getNameGameTypeAttribute()
    {
        return self::TRANSLATE_GAME_TYPE($this->gameType);
    }

    public static function TRANSLATE_GAME_TYPE($gameType)
    {
        switch ($gameType){
            case 'SEARCH_BUBBLES':
                return 'Búsqueda de burbujas';
                break;
            case 'ANSWER_QUESTIONS':
                return 'Responder preguntas';
                break;
            case 'SEARCH_OBJECTS':
                return 'Búsqueda de objetos';
                break;
            default:
                return 'No game Type';
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['idVideo', 'catalogType', 'user_id'];
    protected $hidden = ['created_at', 'updated_at'];
    public function user()
    {
        return$this->belongsTo(User::class);
    }
}

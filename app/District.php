<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['id', 'code', 'name','region_id'];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function sectors()
    {
        return $this->hasMany(Sector::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $fillable = ['id', 'code', 'name','district_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function jedi()
    {
        return $this->hasOne(Jedi::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}

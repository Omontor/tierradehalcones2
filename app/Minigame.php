<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Minigame extends Model
{
    protected $fillable = ['idMinigame','user_id'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = [ 'idMinigame' => 'integer' ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @internal Updateing minigames score
     * @param User $user
     * @return int
     */
    public function updateScore(User $user)
    {
        $user->minigamesScore += 300;
        $user->save();
        $user->updateScore();
        return $user->minigamesScore;
    }

}

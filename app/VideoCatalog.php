<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCatalog extends Model
{
    protected $table = 'video_catalogs';
    protected $fillable = ['catalogType','name','maxNumber','isEnable'];
    protected $casts = [
        'isEnable' => 'boolean',
        'maxNumber' => 'integer'
    ];
}

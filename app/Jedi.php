<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jedi extends Model
{
    protected $table = 'jedis';
    protected $fillable = ['id', 'name', 'sector_id'];

    public  function sector()
    {
        return $this->belongsTo(Sector::class);
    }
}

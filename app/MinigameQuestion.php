<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MinigameQuestion extends Model
{
    protected $table = 'minigame_questions';
    protected $fillable = ['text', 'dynamic_minigame_catalogue_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function dynamicMinigame()
    {
        return $this->belongsTo(DynamicMinigameCatalogue::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}

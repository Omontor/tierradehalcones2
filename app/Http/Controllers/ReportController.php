<?php

namespace App\Http\Controllers;

use App\User;
use App\Feedback;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends Controller
{
    public function userReport($id)
    {
        $user = User::find($id);
        $categories = config('questionsCategories');
        if (!$user){
            return response()->json('User not Found', 404);
        }

        $date = Carbon::now()->format('d_m_y');
        $fileName = "{$user->name}_report_{$date}";
        Excel::create($fileName, function($excel) use ($user, $categories) {

            $excel->setTitle($user->name . 'report');

            $excel->sheet('Preguntas', function($sheet) use($user, $categories){
                $questions = $user->questions;
                $rightQuestions = $questions->reduce(function ($tot, $q) {
                    if ($q->answer === 1){
                        return $tot + 1;
                    }
                    return $tot;
                }, 0);

                $questionsByCategory = $questions->groupBy('category');

                $sheet->loadView('reports.questions', [
                    'categories' => $categories,
                    'user' => $user,
                    'rightQuestions' => $rightQuestions,
                    'questionsByCategory' => $questionsByCategory
                ]);
            });
            $excel->sheet('Burbujas', function($sheet) use($user) {

                $bubbles = $user->bubbles;
                $collected = count($bubbles);
                $simpleBubbles = $bubbles->map(function ($b, $key) {
                    return [
                        $b->idBubble,
                        'Recolectada'
                    ];
                });
                $sheet->row(1,['Burbuja', 'Estado']);
                $sheet->row(1, function($row){
                    $row->setBackground('#E6E6E6');
                });
                $sheet->rows($simpleBubbles);
                $sheet->appendRow(['Recolectadas', $collected]);
            });
            $excel->sheet('Minijuegos', function($sheet) use($user){

                $minigames = $user->minigames;
                $simpleMinigames = $minigames->map(function ($m, $key) {
                    return [
                        $m->idMinigame,
                        'Completado'
                    ];
                });
                $sheet->row(1,['Minijuego', 'Estado']);
                $sheet->row(1, function($row){
                    $row->setBackground('#E6E6E6');
                });
                $sheet->rows($simpleMinigames);
            });

        })->download('xlsx');

    }
    public function globalQuestionsUsers()
    {
        
        
        $date = Carbon::now()->format('d_m_y');
        $fileName = "global_questions_users_report_{$date}";

        Excel::create($fileName, function($excel){
            $excel->setTitle('Global Questions Users report');
            $excel->sheet('Preguntas', function($sheet){
                $firstRow = ['Id','Nombre','Email','1','2','3','4','5','6','7','8','9','10',
                    '11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30',
                    '31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50',
                    '51','52','5','54','55','56','57','58','59','60','61','62','63','6','65','66','67','68','69','70',
                    '71','72','73','7','75','7','77','78','79','80','MERCADEOS','DISPONIBILIDAD','CRITERIOS DE EXHIBICIÓN',
                    'SEGURIDAD','OMS'
                ];
                $sheet->row(1, $firstRow);
                $categories = config('questionsCategories');
                User::where('rol', 'user')->chunk(500, function($users) use ($sheet,$categories){
                    foreach ($users as $user) {
                        $row = [
                            $user->id,
                            $user->name,
                            $user->email
                        ];
                        for ($i=0; $i < 80; $i++) { 
                            $isQuestionExistsAndTrue = $user->questions->contains(function($question, $key) use($i){
                                    return $question->idQuestion == $i && $question->answer == 1;
                                });
                            if ($isQuestionExistsAndTrue) {
                                array_push($row, '✔');
                            } else {
                                array_push($row, '✖');
                            }   
                        }
                        $questionsByCategory = $user->questions->groupBy('category');
                       
                        foreach ($questionsByCategory as $key => $questions){
                            $rightCategoryQuestions = $questions->reduce(function ($tot, $q) {
                                if ($q->answer == 1){
                                return $tot + 1;
                                }
                                return $tot;
                            }, 0);
                            array_push($row, "{$rightCategoryQuestions} de {$categories[$key]['total']}");
                        }
                        $sheet->appendRow($row);
                    }
                });
                
            });
        })->download('xlsx');
    }

    public function globalBubblesUsers()
    {
        $users = User::where('rol', 'user')->get();
        $date = Carbon::now()->format('d_m_y');
        $fileName = "global_bubbles_users_report_{$date}";
        $numBubbles = collect([]);
        for ($i=1; $i <=40 ; $i++) {
            $numBubbles->push($i);
        }
        Excel::create($fileName, function($excel) use ($users, $numBubbles){
            $excel->setTitle('Global Bubbles Users report');
            $excel->sheet('Burbujas', function($sheet) use($users, $numBubbles){

                $header = ['ID', 'Nombre', 'Email', 'B1', 'B2', 'B3','B4',
                    'B5', 'B6','B7', 'B8', 'B9','B10', 'B11', 'B12','B13',
                    'B14', 'B15','B16', 'B17', 'B18','B19', 'B20', 'B21','B22', 'B23',
                    'B24', 'B25','B26', 'B27', 'B28','B29', 'B30', 'B31','B32', 'B33',
                    'B34', 'B35','B36', 'B37', 'B38','B39', 'B40', 'Total'
                ];
                $sheet->row(1,$header);
                foreach ($users as $user) {

                    $bubbles = $numBubbles->map(function($b) use($user){
                        if ($user->bubbles->contains('idBubble', $b)) {
                            return '✔';
                        } else {
                            return '';
                        }
                    });

                    $numCollected = $bubbles->reduce(function ($tot, $b) {
                        if ($b === '✔'){
                            return $tot + 1;
                        }
                        return $tot;
                    }, 0);
                    $myRow = collect([$user->id, $user->name, $user->email]);

                    $sheet->appendRow($myRow->concat($bubbles)->concat([$numCollected])->toArray());
                }
            });
        })->download('xlsx');
    }
    public function globalMinigamesUsers()
    {
        $users = User::where('rol', 'user')->get();
        $date = Carbon::now()->format('d_m_y');
        $fileName = "global_minigames_users_report_{$date}";

        Excel::create($fileName, function($excel) use ($users){
            $excel->sheet('Minigames', function($sheet) use($users){
                $sheet->appendRow(['ID', 'Name', 'Email', 'Minigame 1', 'Minigame 2', 'Minigame 3', 'Minigame 4', 'Minigame 5','Minigame 6', 'Minigame 7', 'Minigame 8']);
                foreach ($users as $user) {
                    $minigames = collect([1,2,3,4,5,6,7,8])->map(function ($m) use($user) {
                        if ($user->minigames->contains('idMinigame', $m)) {
                            return 'Completado';
                        } else {
                            return '';
                        }
                    });
                    $row = collect([$user->id, $user->name, $user->email])->concat($minigames)->toArray();
                    $sheet->appendRow($row);
                }
            });
        })->download('xlsx');
    }

    public function usersGameover(){
        $feedbacks = Feedback::where('feedback', 'LIKE','%ha terminado el juego el%')->get();
        $date = Carbon::now()->format('d_m_y');
        $fileName = "users_report_gameover_{$date}";

        Excel::create($fileName, function($excel) use($feedbacks){
            $excel->sheet('Users complete game', function($sheet) use($feedbacks){
                $sheet->appendRow(['ID', 'Fecha']);
                foreach ($feedbacks as $feedback) {
                    $split = explode('ha terminado el juego el', $feedback->feedback);
                    $sheet->appendRow([$split[0],$split[1]]);
                }
            });
        })->download('xlsx');
    }

    public function reportTopUsers()
    {
        $topTen = User::where('rol', 'user')
            ->with(['questions','bubbles','minigames','dynamicMinigames','videos',
                'sector'=> function($query){
                    $query->with(['jedi',
                        'district' => function($query){
                            $query->with(['region']);
                        }
                    ]);
                }])
            ->orderBy('score','desc')->take(100)->get();
        $date = Carbon::now()->format('d_m_y');
        $fileName = "users_report_top_scores_{$date}";

        Excel::create($fileName, function($excel) use($topTen){
            $excel->sheet('Users complete game', function($sheet) use($topTen){
                $sheet->appendRow(['ID', 'Usuario', 'Region', 'Distrito', 'Sector', 'Status', 'Jedi', 'Nombre completo',
                    'Inicio Sesión','Puntos', 'Burbujas', 'Preguntas Correctas', 'Preguntas Incorrectas', 'Coaching',
                    'Minijuegos', 'Juegos Online(dinámicos)', 'Capacitaciones','Elearnings', 'Videos Vistos']);

                foreach ($topTen as $user) {
                    $questions = $user->questions;
                    $rightQuestions = $questions->reduce(function ($tot, $q) {
                        if ($q->answer === 1){
                            return $tot + 1;
                        }
                        return $tot;
                    }, 0);
                    $sheet->appendRow([
                        $user->id,
                        $user->name,
                        (isset($user->sector)) ? $user->sector->district->region->code : 'N/A',
                        (isset($user->sector)) ? $user->sector->district->code: 'N/A',
                        (isset($user->sector)) ? $user->sector->code: 'N/A',
                        ($user->score > 0) ? 'Activo': 'Inactivo',
                        (isset($user->sector)) ? $user->sector->jedi->name: 'N/A',
                        $user->completeName,
                        ($user->score > 0) ? 'Activo': 'Inactivo',
                        $user->score,
                        $user->bubbleScore,
                        $rightQuestions,
                        count($questions) - $rightQuestions,
                        $user->videos()->where('catalogType', 'COACHING')->count(),
                        count($user->minigames),
                        count($user->dynamicMinigames),
                        $user->videos()->where('catalogType', 'TRAINING')->count(),
                        $user->videos()->where('catalogType', 'ELEARNING')->count(),
                        $user->videosSeenNumber
                    ]);
                }
            });
        })->download('xlsx');
    }

    public function generalReport(Request $request)
    {
        $sectorId = $request->query('sector');
        $users = User::where('rol', 'user')
            ->with(['questions','bubbles','minigames','dynamicMinigames','videos',
                'sector'=> function($query){
                    $query->with(['jedi',
                        'district' => function($query){
                            $query->with(['region']);
                        }
                    ]);
                }])
            ->where('sector_id', $sectorId)
            ->orderBy('completeNAme','desc')->take(100)->get();

        $date = Carbon::now()->format('d_m_y');
        $fileName = "users_general_report_{$date}";
        Excel::create($fileName, function($excel) use($users){
            $excel->sheet('Users complete game', function($sheet) use($users){
                $sheet->appendRow(['ID', 'Usuario', 'Region', 'Distrito', 'Sector', 'Status', 'Jedi', 'Nombre completo',
                    'Inicio Sesión','Puntos', 'Burbujas', 'Preguntas Correctas', 'Preguntas Incorrectas', 'Coaching',
                    'Minijuegos', 'Juegos Online(dinámicos)', 'Capacitaciones','Elearnings', 'Videos Vistos']);

                foreach ($users as $user) {
                    $questions = $user->questions;
                    $rightQuestions = $questions->reduce(function ($tot, $q) {
                        if ($q->answer === 1){
                            return $tot + 1;
                        }
                        return $tot;
                    }, 0);
                    $sheet->appendRow([
                        $user->id,
                        $user->name,
                        (isset($user->sector)) ? $user->sector->district->region->code : 'N/A',
                        (isset($user->sector)) ? $user->sector->district->code: 'N/A',
                        (isset($user->sector)) ? $user->sector->code: 'N/A',
                        ($user->score > 0) ? 'Activo': 'Inactivo',
                        (isset($user->sector)) ? $user->sector->jedi->name: 'N/A',
                        $user->completeName,
                        ($user->score > 0) ? 'Activo': 'Inactivo',
                        $user->score,
                        $user->bubbleScore,
                        $rightQuestions,
                        count($questions) - $rightQuestions,
                        $user->videos()->where('catalogType', 'COACHING')->count(),
                        count($user->minigames),
                        count($user->dynamicMinigames),
                        $user->videos()->where('catalogType', 'TRAINING')->count(),
                        $user->videos()->where('catalogType', 'ELEARNING')->count(),
                        $user->videosSeenNumber
                    ]);
                }
            });
        })->download('xlsx');
    }
}

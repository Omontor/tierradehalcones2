<?php

namespace App\Http\Controllers;

use App\District;
use App\Region;
use App\Sector;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function regionsList()
    {
        $regions = Region::orderBy('created_at','desc')->get();
        return response()->json($regions, 200);
    }

    public function districtsList($regionId)
    {
        $districts = District::where('region_id', $regionId)->orderBy('created_at','desc')->get();
        return response()->json($districts, 200);
    }

    public function sectorsList($regionId, $districtId)
    {
        $sectors = Sector::where('district_id', $districtId)->orderBy('created_at','desc')->get();
        return response()->json($sectors, 200);
    }

    /**
     * It retrives the direct list of sectors
     * @return \Illuminate\Http\JsonResponse
     */
    public function sectors()
    {
        $sectors = Sector::all();

        return response()->json($sectors, 200);
    }
}

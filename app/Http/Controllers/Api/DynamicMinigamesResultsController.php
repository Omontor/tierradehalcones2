<?php

namespace App\Http\Controllers\Api;

use App\DynamicMinigame;
use App\DynamicMinigameCatalogue;
use App\Target;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DynamicMinigamesResultsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $dynamicMinigames = $user->dynamicMinigames()->with(['dynamicMinigameCatalogue'])->get();

        return response()->json(['data' => $dynamicMinigames], 200);
    }
    public function show($id)
    {
        $minigame = DynamicMinigame::where('id', $id)->with(['dynamicMinigameCatalogue','targets'])->first();

        return response()->json(['data' => $minigame], 200);
    }

    public function store(Request $request)
    {
        try{
            $user = $request->user();
            /**
             * Start Validation
             **/
            $validator = Validator::make($request->all(), [
                'targetId' => 'required|integer',
                'gameType' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
            $dynamicMinigameCatalogue = DynamicMinigameCatalogue::where('gameType', $request->gameType)->first();
            if (!$dynamicMinigameCatalogue){
                return response()->json([
                    'error' => 'NOT FOUND',
                    'message' => 'Este Juego dinámico no existe'
                ], 404);
            }
            $validator = Validator::make($request->all(), [
                'targetId' => "required|integer|between:1,{$dynamicMinigameCatalogue->unitNumber}",
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }
            /**
             * End Validation
            **/
            $dynamicMinigame = DynamicMinigame::where('dynamic_minigame_catalogue_id', $dynamicMinigameCatalogue->id)
                ->where('user_id', $user->id)->first();

            if (!$dynamicMinigame){
                $dynamicMinigame = DynamicMinigame::create([
                    'unitType' => $dynamicMinigameCatalogue->unitType,
                    'totalTargets' => $dynamicMinigameCatalogue->unitNumber,
                    'user_id' => $user->id,
                    'dynamic_minigame_catalogue_id' => $dynamicMinigameCatalogue->id
                ]);
            }

            $target = Target::where('targetId' , $request->targetId)
                ->where('dynamic_minigames_id', $dynamicMinigame->id)
                ->first();

            if(!$target){
                $target = Target::create([
                    'targetId' => $request->targetId,
                    'dynamic_minigames_id' => $dynamicMinigame->id,
                    'user_id' => $user->id
                ]);
                $dynamicMinigame->updateScore();
                $user->dynamicMinigamesScore($dynamicMinigameCatalogue->gameType);
            }
            return response()->json($target, 200, [], JSON_NUMERIC_CHECK);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}

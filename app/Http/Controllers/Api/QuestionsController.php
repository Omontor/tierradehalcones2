<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class QuestionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $user = $request->user();
        $questions = $user->questions->makeHidden(['user_id','id']);
       return response()->json(['data' => $questions], 200,[], JSON_NUMERIC_CHECK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = $request->user();
            $categories = config('allQuestionsCategory');

            $validator = Validator::make($request->all(), [
                'idQuestion' => 'required|integer|between:1,80',
                'answer' => 'required|boolean', //true,  false, 1, 0, "1", and "0"
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            }

            $question = Question::where('idQuestion' , $request->idQuestion)->where('user_id' , $user->id)->first();

            if($question){
                $oldValue = $question->answer;
                $question->answer = filter_var($request->answer, FILTER_VALIDATE_BOOLEAN);
                $question->save();
                $question->updateScore($user,$oldValue);
            } else{
                $question = Question::create([
                    'idQuestion' => $request->idQuestion,
                    'answer' => filter_var($request->answer, FILTER_VALIDATE_BOOLEAN),
                    'category' => $categories[$request->idQuestion],
                    'user_id' => $user->id
                ]);
                $question->updateScore($user);
            }

            return response()->json($question, 200,[], JSON_NUMERIC_CHECK);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

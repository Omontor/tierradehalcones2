<?php

namespace App\Http\Controllers\Api;

use App\DynamicMinigameCatalogue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DynamicMinigameController extends Controller
{
    public function listMinigames()
    {
        $today = Carbon::now();
        $minigames = DynamicMinigameCatalogue::with([
            'questions' => function($query){
                $query->with(['answers']);
            }
        ])->where('maturityDate', '>=', $today)->get();

        return response()->json(['data' => $minigames], 200);
    }
    public function show($id)
    {
        $minigame = DynamicMinigameCatalogue::where('id', $id)->with([
            'questions' => function($query){
                $query->with(['answers']);
            }
        ])->first();

        if (!$minigame){
            return response()->json([
                'error' => 'NOT FOUND',
                'message' => 'Este Juego dinámico no existe'
            ], 404);
        }

        return response()->json(['data' => $minigame], 200);
    }
}

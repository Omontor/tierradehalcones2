<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Minigame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MiniGamesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $minigames = $user->minigames->makeHidden(['user_id','id']);
        return response()->json(['data' => $minigames],200,[], JSON_NUMERIC_CHECK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = $request->user();

            $validator = Validator::make($request->all(), [
                'idMinigame' => 'required|integer|between:1,8',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 422);
            }

            $minigame = Minigame::where('idMinigame' , $request->idMinigame)->where( 'user_id' , $user->id)->first();
            if(!$minigame){
                $minigame = Minigame::create([
                    'idMinigame' => $request->idMinigame,
                    'user_id' => $user->id
                ]);
                $minigame->updateScore($user);
            }
            return response()->json($minigame, 200,[], JSON_NUMERIC_CHECK);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function topTenList()
    {
        $topTen = User::where('rol', 'user')->with(['questions','bubbles','minigames'])->orderBy('score','desc')->take(10)->get();
        return response()->json(['data' => $topTen],200,[], JSON_NUMERIC_CHECK);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Video;
use App\VideoCatalog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VideosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(Request $request)
    {
       try{
           $user = $request->user();

           $validator = Validator::make($request->all(), [
               'idVideo' => 'required|integer',
               'catalogType' => 'required'
           ]);
           if ($validator->fails()) {
               return response()->json($validator->errors(), 400);
           }

           $videoCatalog = VideoCatalog::where('catalogType', $request->catalogType)->first();
           if (!$videoCatalog){
               return response()->json([
                   'error' => 'NOT FOUND',
                   'message' => 'Este tipo de Videos no existe'
               ], 404);
           }

           $validator = Validator::make($request->all(), [
               'idVideo' => 'required|integer|between:1,'.$videoCatalog->maxNumber,
           ]);
           if ($validator->fails()) {
               return response()->json($validator->errors(), 400);
           }

           $video = Video::where('idVideo' , $request->idVideo)
               ->where('catalogType', $request->catalogType)
               ->where('user_id' , $user->id)
               ->first();

           if(! $video){
               $video = Video::create([
                   'idVideo' => $request->idVideo,
                   'catalogType' => $request->catalogType,
                   'user_id' => $user->id
               ]);
               $user->videosSeenNumber = $user->videosSeenNumber + 1;
               $user->save();
           }

           return response()->json($video, 201,[], JSON_NUMERIC_CHECK);
       } catch (\Exception $e) {
           return response()->json(['error' => $e->getMessage()], 500);
       }
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $videos = Video::where('user_id', $user->id);

        if ($request->query('catalogType')){
            $videos = $videos->where('catalogType', $request->query('catalogType'));
        }

        $videos = $videos->orderBy('catalogType', 'desc')->get();

        return response()->json(['data' => $videos], 200);
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function gameOver(Request $request)
    {
        $user = $request->user();

        $title = $user->name . ' ha terminado el juego !!';
        $message = $user->name. ' ha terminado el juego completamente y tú ya casi acabas ? podrás superar su record?';

        $content = ["en" => $message];

        $fields = [
            'app_id' => env('ONESIGNAL_APP_ID_DEV'),
            'included_segments' => ['All'],
            'contents' => $content,
            'large_icon' => 'http://tierradehalcones.com/images/badge.jpg',
//            'url' => $request->link,
            'headings' => ['en' => $title],
            //'subtitle' => ['en' => $request->title]
        ];

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic ' . env('ONESIGNAL_API_KEY_DEV')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $response = json_decode($response);
        if ($httpcode == 200) {
            if (true) {
                return response()->json([
                    'message' => 'Notification sent'
                ], 200);
            } else {
                return response()->json([
                    'error' => $response->errors[0]
                ], 500);
            }
        }
    }
}

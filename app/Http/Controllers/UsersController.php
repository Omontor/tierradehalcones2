<?php

namespace App\Http\Controllers;

use App\DynamicMinigameCatalogue;
use App\Minigame;
use App\Sector;
use App\User;
use App\Question;
use App\Bubble;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::where('rol', 'user')->with(['questions','bubbles','minigames','sector',
            'dynamicMinigames' => function($query){
                $query->with(['dynamicMinigameCatalogue']);
            }
        ]);

        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $users = $users
                ->where('name', 'like', '%' . $request->query('name') . '%');
        }
        if($request->query('email') && $request->query('filter') !== 'undefined'){
            $users = $users
                ->where('email', 'like', '%' . $request->query('email') . '%');
        }
        $users = $users->paginate(10);
        return response()->json($users,200,[], JSON_NUMERIC_CHECK);
    }

    public function viewTopTen()
    {
        return view('toptens');
    }

    public function viewGlobalStats()
    {
        return view('globalStats');
    }

    public function usersList()
    {
        return view('users');
    }

    public function topTenList()
    {
        $topTen = User::where('rol', 'user')->with(['questions','bubbles','minigames'])->orderBy('score','desc')->take(100)->get();
        return response()->json($topTen,200,[], JSON_NUMERIC_CHECK);
    }

    public function create()
    {
        return view('newUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'completeName' => 'required|string',
            'sector' => 'required|integer',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors(), 400); //Unprocessable Data
        }
        $sector = Sector::find($request->sector);
        $user = User::create([
            'name' => $request->name,
            'completeName' => $request->completeName,
            'email' => $request->email,
            'sector_id' => $sector->id,
            'password' => Hash::make($request->password)
        ]);

        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($request->name){
            $user->name = $request->name;
        }
        if ($request->name){
            $user->completeName = $request->completeName;
        }
        if ($request->password){
            $user->password = Hash::make($request->password);
        }
        if ($request->sector){
            $user->sector_id = $request->sector;
        }

        $user->save();
        $user = User::where('id', $id)->with(['questions','bubbles','minigames','sector'])->first();

        return response()->json($user, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $deletedQuestions   = Question::where('user_id', $id)->delete();
        $deletedBubbles     = Bubble::where('user_id', $id)->delete();
        $deletedMinigames   = Minigame::where('user_id', $id)->delete();
        $user->delete();

        return response()->json($user);
    }
    public function globalStatsQuestionsByCategory()
    {
        try{
            $questionCategories = config('questionsCategories');
            $categoriesStats = [];

            foreach ($questionCategories as $key => $category){
                array_push($categoriesStats, Question::percentagesSuccessCategory($key));
            }

            return response()->json($categoriesStats);

        } catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function globalStatsQuestions()
    {
        $results = Question::percentagesSuccessQuestions();

        return response()->json([
            'numRightAnswers' => $results['numRightAnswers'],
            'wrongAnswers' => $results['wrongAnswers'],
            'supposedQuestions' => $results['supposedQuestions'],
            'users' => $results['users'],
            'accuratePercentage' => $results['accuratePercentage'],
            'wrongPercentage' => $results['wrongPercentage']
        ], 200);

    }

    public function globalStatsBubbles()
    {
        $results = Bubble::percentagesRecollectedBubbles();

        return response()->json([
            'numBubblesCollected' => $results['numBubblesCollected'],
            'missingBubbles' => $results['missingBubbles'],
            'supposedBubbles' => $results['supposedBubbles'],
            'users' => $results['users'],
            'recollectedPercentage' => $results['recollectedPercentage'],
            'missingPercentage' => $results['missingPercentage']
        ], 200);


    }

    public function globalStatsdynamicMinigames()
    {
        $dynamicMinigamesCatalogue = DynamicMinigameCatalogue::all();
        $totalUsers = User::where('rol', 'user')->count();
        $minigamesStats = collect();

        $dynamicMinigamesCatalogue->each(function($minigame) use ($minigamesStats, $totalUsers){
            $playedTimes = User::whereHas('dynamicMinigames', function($query) use($minigame){
                $query->whereHas('dynamicMinigameCatalogue', function($query) use($minigame){
                   $query->where('gameType',$minigame->gameType);
                });
            })->count();
            $timesPlayedPercentage = $playedTimes * 100 / $totalUsers;
            $timesNotPlayedPercentage = 100 - $timesPlayedPercentage;
            $minigamesStats->push([
                'gameType' => $minigame->gameType,
                'gameTypeName' => DynamicMinigameCatalogue::TRANSLATE_GAME_TYPE($minigame->gameType),
                'timesPlayedPercentage' => $timesPlayedPercentage,
                'timesNotPlayedPercentage' => $timesNotPlayedPercentage,
            ]);
        });

        return response()->json([
            'minigamesStats' => $minigamesStats
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Bubble;
use App\District;
use App\Jedi;
use App\Minigame;
use App\Question;
use App\Region;
use App\Sector;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{

    public function filesView()
    {
        return view('fileOperations');
    }
    public function registerUsers(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'excel_creator' => 'required|file'
            ]);
            if ($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()
                ]);
            }
            $errors = [];

            $excel = $request->file('excel_creator');
            $excelName = str_random(15).'_'.$excel->getFilename().'.'.$excel->getClientOriginalExtension();
            $excel->move(public_path('excels'), $excelName);

            $excel = Excel::load(public_path('excels/'.$excelName),function($reader) { })->get();
            $excelUsers = $excel->toArray();
            foreach ($excelUsers as $index => $exUser){
                $validator = Validator::make($exUser, [
                    'nombre' => 'required|string|max:255',
                    'email' => 'required|string|email|unique:users',
                    'contrasena' => 'required|string|min:6',
                    'nombre_completo' => 'required|string',
                    'sector' => 'required',
                ]);

                if ($validator->fails()){
                    array_push($errors, [
                        'row' => $index+1,
                        'errors' => $validator->errors()
                    ]);
                } else {
                    $sector = Sector::where('name', $exUser['sector'])->first();

                    if (!$sector){
                        array_push($errors, [
                            'row' => $index+1,
                            'errors' => ["El sector: {$exUser['sector']} NO existe"]
                        ]);
                    } else{
                        User::create([
                            'name' => $exUser['nombre'],
                            'completeName' => $exUser['nombre_completo'],
                            'email' => $exUser['email'],
                            'sector_id' => $sector->id,
                            'password' => bcrypt($exUser['contrasena'])
                        ]);
                    }
                }
            }
            if (count($errors)>0){
                return response()->json([
                    'errors' => $errors
                ],409); //409
            }

            return response()->json('NO CONTENT', 204);


        } catch (\Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'friendly' => "Algo malo paso"
            ]);
        }
    }

    public function deleteUsers(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'excel_deletor' => 'required|file'
            ]);
            if ($validator->fails()){
                return response()->json([
                    'errors' => $validator->errors()
                ]);
            }
            $errors = [];
            $excel = $request->file('excel_deletor');
            $excelName = str_random(15).'_'.$excel->getFilename().'.'.$excel->getClientOriginalExtension();
            $excel->move(public_path('excels'), $excelName);

            $excel = Excel::load(public_path('excels/'.$excelName),function($reader) { })->get();
            $excelUsers = $excel->toArray();
            foreach ($excelUsers as $index => $exUser){
                $validator = Validator::make($exUser, [
                    'nombre' => 'required|string',
                ]);

                if ($validator->fails()){
                    array_push($errors, [
                        'row' => $index+1,
                        'errors' => $validator->errors()
                    ]);
                } else {
                    $user = User::where('name',$exUser['nombre'])->first();
                    if (!$user){
                        array_push($errors, [
                            'row' => $index+1,
                            'errors' => ["El usuario {$exUser['nombre']} no existe"]
                        ]);
                    } else {
                        $deletedQuestions   = Question::where('user_id', $user->id)->delete();
                        $deletedBubbles     = Bubble::where('user_id', $user->id)->delete();
                        $deletedMinigames   = Minigame::where('user_id', $user->id)->delete();
                        $user->delete();
                    }
                }
            }
            if (count($errors)>0){
                return response()->json([
                    'errors' => $errors
                ], 409); //CONFLICT
            }

            return response()->json('NO CONTENT', 204);
        }catch ( \Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'friendly' => "Algo malo paso"
            ]);
        }
    }

    public function registerLocations()
    {
        try{
            $excel = Excel::load(public_path('LocationAreas.xlsx'),function($reader) { })->get();
            $areas = $excel->toArray();
            foreach ($areas as $index => $area){
                $region = Region::firstOrCreate([
                    'code' => $area['region'],
                    'name' => $area['region']
                ]);
                $district = District::firstOrCreate([
                    'code' => $area['distrito'],
                    'name' => $area['distrito'],
                    'region_id' => $region->id
                ]);
                $sector = Sector::firstOrCreate([
                    'code' => $area['sector'],
                    'name' => $area['sector'],
                    'district_id' => $district->id
                ]);
                $jedi = Jedi::firstOrCreate([
                    'name' => $area['jefe_de_distrito'],
                    'sector_id' => $sector->id
                ]);
            }
            return response()->json('NO CONTENT', 204);
        }catch (\Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'friendly' => "Algo malo paso"
            ]);
        }
    }

    public function fillSectorToUsers()
    {
        try{
            $excel = Excel::load(public_path('usersFull.xlsx'),function($reader) { })->get();
            $usersExcel = $excel->toArray();
            foreach ($usersExcel as $userExe) {
                $numEmployee = (string) $userExe['n0_de_empleado'];
                $user = User::where('name', $numEmployee)->first();
                $sector = Sector::where('name',$userExe['sector'])->first();
                $user->sector_id = $sector->id;
                $user->save();
            }
            return response()->json('NO CONTENT', 204);
        }catch (\Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'friendly' => "Algo malo paso"
            ]);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Answer;
use App\DynamicMinigameCatalogue;
use App\MinigameQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DynamicMinigameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewCreation()
    {
        return view('createDynamicMinigame');
    }

    public function viewMinigames()
    {
        return view('dynamicMinigames');
    }

    public function listMinigames()
    {
        $minigames = DynamicMinigameCatalogue::with([
            'questions' => function($query){
                $query->with(['answers']);
            }
        ])->orderBy('created_at', 'desc')->get();

        return response()->json($minigames, 200);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'          => 'required|string|max:120',
            'maturityDate'  => 'required|date',
            'duration'      => 'required|integer',
            'gameType'      => 'required|in:SEARCH_BUBBLES,ANSWER_QUESTIONS,SEARCH_OBJECTS',
            'unitNumber'    => 'required|integer',
            'questions'     => 'required_if:gameType,ANSWER_QUESTIONS'
        ]);

        if($validation->fails()){
            return response()->json($validation->errors(), 400); //Unprocessable Data
        }
        $unitType = DynamicMinigameCatalogue::extractUnitType($request->gameType);

        $minigame = DynamicMinigameCatalogue::create([
            'name'          => $request->name,
            'maturityDate'  => $request->maturityDate,
            'duration'      => $request->duration,
            'gameType'      => $request->gameType,
            'unitNumber'    => $request->unitNumber,
            'unitType'      => $unitType,
            'isActive'      => true
        ]);

        if ($minigame->isAnswerQuestionType()){
            $questions = $request->questions;
            foreach ($questions as $q){
                $question = MinigameQuestion::create([
                    'text' => $q['text'],
                    'dynamic_minigame_catalogue_id' => $minigame->id
                ]);
                foreach ($q['answers'] as $ans){
                    $answer = Answer::create([
                        'text' => $ans['text'],
                        'isAnswer' => $ans['isAnswer'],
                        'minigame_question_id' => $question->id
                    ]);
                }
            }
        }

        return response()->json($minigame, 201);
    }
}

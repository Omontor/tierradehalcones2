<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Integer;

class Question extends Model
{
    protected $fillable = ['idQuestion', 'answer','user_id', 'category'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = ['idQuestion' => 'integer', 'user_id' => 'integer'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param User $user
     * @param int $oldValue
     * @return int Question Score
     */
    public function updateScore(User $user, $oldValue = null)
    {
        //removing the previous points if answer is updated
        if (isset($oldValue)){
            if (filter_var($oldValue, FILTER_VALIDATE_BOOLEAN)){
                $user->questionsScore -= 30;
            }else {
                $user->questionsScore += 15;
            }
        }
        if (filter_var($this->answer, FILTER_VALIDATE_BOOLEAN)){
            $user->questionsScore += 30;
        } else {
            $user->questionsScore -= 15;
        }
        $user->save();
        $user->updateScore();
        return $user->questionsScore;
    }
    public static function percentagesSuccessCategory($category)
    {
        if (!$category) throw new \Exception('Missing category');
        $categories = config('questionsCategories');

        $numRightAnswers = Question::where('category', $category)->where('answer', 1)->count();
        $users = User::where('rol','user')->count();
        $supposedQuestions = $users * $categories[$category]['total'];
        $wrongAnswers = $supposedQuestions - $numRightAnswers;
        $accuratePercentage = $numRightAnswers * 100 / $supposedQuestions;
        $wrongPercentage = 100 - $accuratePercentage;
        return [
            'category' => $category,
            'numRightAnswers' => $numRightAnswers,
            'wrongAnswers' => $wrongAnswers,
            'supposedQuestions' => $supposedQuestions,
            'users' => $users,
            'accuratePercentage' => $accuratePercentage,
            'wrongPercentage' => $wrongPercentage
        ];
    }
    public static function percentagesSuccessQuestions()
    {
      $numRightAnswers = Question::where('answer', 1)->count();
      $users = User::where('rol','user')->count();
      $supposedQuestions = $users * 80;
      $wrongAnswers = $supposedQuestions - $numRightAnswers;

      $accuratePercentage = $numRightAnswers * 100 / $supposedQuestions;
      $wrongPercentage = 100 - $accuratePercentage;

      return [
        'numRightAnswers' => $numRightAnswers,
        'wrongAnswers' => $wrongAnswers,
        'supposedQuestions' => $supposedQuestions,
        'users' => $users,
        'accuratePercentage' => $accuratePercentage,
        'wrongPercentage' => $wrongPercentage
      ];
    }
}

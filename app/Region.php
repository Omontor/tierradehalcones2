<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['id', 'code', 'name'];

    public function districts()
    {
        return $this->hasMany(District::class);
    }
}

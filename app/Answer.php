<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['text', 'isAnswer', 'minigame_question_id'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = [
        'isAnswer' => 'boolean'
    ];
    public function question()
    {
        return $this->belongsTo(MinigameQuestion::class);
    }
}
